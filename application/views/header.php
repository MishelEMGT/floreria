<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FLORERIA</title>
    <!-- Latest compiled and minified CSS -->
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
            <!-- importar query -->

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-cunRm6r-G4XEg0wLrw3AHPbQJSxb8fw&libraries=places&callback=initMap">
    </script>

  </head>
  <body>

   </nav>
   <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
     <div class="container-fluid">
       <div class="navbar-header">
         <a class="navbar-brand" href=" <?php echo site_url();?> ">INICIO</a>
       </div>

       <ul class="nav navbar-nav">
         <li><a href="<?php echo site_url();?>/paginas/catalogo">CATALOGO</a></li>
         <li><a href="<?php echo site_url();?>/paginas/misionyvision">MISION Y VISION</a></li>
         <li><a href="<?php echo site_url();?>/paginas/contactenos">CONTACTENOS</a></li>
         <li class="dropdown">
           <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo site_url();?>/paginas/productos">USUARIOS
           <span class="caret"></span></a>

           <ul class="dropdown-menu">
             <li><a href="<?php echo site_url();?>/paginas/nuevo">Registrarse</a></li>
           </ul>


         </li>
       </ul>

     </div>
 </nav>
