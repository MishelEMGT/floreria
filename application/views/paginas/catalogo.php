<!--Contenido de la vista catálogo-->
<div class="col-md-12 text-center" style="color:white">
    <h1>PRODUCTOS</h1>
</div>
<div style="background-color:black">
    <div class="container mt-2 contenedor-targetas ">
        <div class="row">
            <div class="col-md-3 espacio">
                <div class="card card-block" style="background-color:white" >
                        <img src="<?php echo base_url();?>/assets/images/rosa1.jpg" alt="" width="260px" height="300px">

                        <div class="contenedor-card">
                        <h5 class="card-title mt-3 mb-3 text-center">PRODUCTO Nº1</h5>
                        <p><h5 class="text-center" > <b>GIRASOLES</b> </h5>
                            <b>Descripcion:</b>Un ramo de girasoles<br>
                            <b>Precio: </b> 3,00$ <br>
                        <p>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </div>
                </div>

            </div>
            <div class="col-md-3 espacio">
                <div class="card card-block" style="background-color:white" >
                        <img src="<?php echo base_url();?>/assets/images/rosa2.jpg" alt="" width="260px" height="300px">

                        <div class="contenedor-card">
                        <h5 class="card-title mt-3 mb-3 text-center">PRODUCTO Nº2</h5>
                        <p> <h5 class="text-center" > <b>CLAVELES</b> </h5>

                            <b>Descripcion:</b>Ramo de claveles<br>
                            <b>Precio </b> 2,00$ <br>
                        <p>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </div>
                </div>

            </div>
            <div class="col-md-3 espacio">
                <div class="card card-block" style="background-color:white" >
                        <img src="<?php echo base_url();?>/assets/images/rosa3.jpg" alt="" width="260px" height="300px">

                        <div class="contenedor-card">
                        <h5 class="card-title mt-3 mb-3 text-center">PRODUCTO Nº3</h5>
                        <p><h5 class="text-center" > <b>ROSAS</b> </h5>
                            <b>Descripcion:</b> Bonche de rosas rojas<br>
                            <b>Precio: </b> 3,00$ <br>
                        <p>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </div>
                </div>

            </div>
            <div class="col-md-3 espacio">
                <div class="card card-block" style="background-color:white" >
                        <img src="<?php echo base_url();?>/assets/images/rosa4.jpg" alt="" width="260px" height="300px">

                        <div class="contenedor-card">
                        <h5 class="card-title mt-3 mb-3 text-center">PRODUCTO Nº4</h5>
                        <p><h5 class="text-center" > <b>ROSAS BLACAS </b> </h5>
                            <b>Descripcion:</b>2 Bonches de rosas blancas <br>
                            <b>Precio: </b> 2,75$ <br>
                        <p>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
