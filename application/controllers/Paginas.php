<?php
  class Paginas extends CI_Controller
  {

    	function __construct()
      {
        parent::__construct();
      }
      public function productos(){
    		$this->load->view('header');
    		$this->load->view('paginas/productos');
    		$this->load->view('footer');
    	}
      public function catalogo(){
    		$this->load->view('header');
    		$this->load->view('paginas/catalogo');
    		$this->load->view('footer');
    	}
      public function misionyvision(){
    		$this->load->view('header');
    		$this->load->view('paginas/misionyvision');
    		$this->load->view('footer');
    	}
      public function contactenos(){
    		$this->load->view('header');
    		$this->load->view('paginas/contactenos');
    		$this->load->view('footer');
    	}
      public function nuevo(){
      $this->load->view('header');
  		$this->load->view('paginas/nuevo');
  		$this->load->view('footer');
    }
    public function index(){
    $this->load->view('header');
    $this->load->view('welcome_message');
    $this->load->view('footer');
  }
  }
?>
